# Stages:
#   builder - lint source, install w/deps
#   base    - Copy python lib from builder
#   debug   - Install debug tools on base

###############################################################################
#
###############################################################################
FROM autovlab/autovlab:pip as builder

USER autovlab
RUN python -m pip install --user --no-warn-script-location --no-cache-dir pylint && \
    python -m pip install --user --no-warn-script-location --no-cache-dir pylint-flask && \
    python -m pip install --user --no-warn-script-location --no-cache-dir pylint-flask-sqlalchemy

# Install pip dependencies first so the layers can be cached 
# We only need to reinstall dependencies if setup.py changed
COPY setup.py /home/autovlab/
RUN python ~/setup.py egg_info -e ~/ && \
    python -m pip install --user --no-warn-script-location --no-cache-dir -r ~/*.egg-info/requires.txt

# Copy in our source
COPY . /home/autovlab/src
WORKDIR /home/autovlab/src

# Fix permissions for autovlab
USER root
RUN chown autovlab:autovlab -R .
USER autovlab

# Lint + install + uninstall linter
# (Find the name of the python package dir by finding __init__.py)
RUN python -m pip install --user --no-warn-script-location --no-cache-dir . && \
    python -m pylint --load-plugins pylint_flask_sqlalchemy pylint_flask -E "$(dirname $(find . -maxdepth 2 -name __init__.py))" && \
    python setup.py bdist_wheel --dist-dir ~/dist && \
    python setup.py sdist --dist-dir ~/dist && \
    python -m pip uninstall -y pylint && \
    python -m pip uninstall -y pylint-flask && \
    python -m pip uninstall -y pylint-flask-sqlalchemy 

###############################################################################
# prod / run image
###############################################################################
FROM python:slim as base

# Add non-root user and switch to it
RUN useradd -ms /bin/bash autovlab
USER autovlab

# Copy the python install from builder (which includes all dependenices)
WORKDIR /home/autovlab

# Copy packer dependencies
COPY --from=autovlab/packinator:base /usr/local/bin/packer /usr/local/bin/
COPY --from=autovlab/packinator:base /usr/bin/jigdo-lite /usr/bin/

# Copy terraform dependencies
COPY --from=autovlab/terradactsl:base /usr/local/bin/terraform /usr/local/bin/
COPY --from=autovlab/terradactsl:base /home/autovlab/.terraform.d/ /home/autovlab/

# Copy python dependencies
COPY --from=builder /home/autovlab/.local /home/autovlab/.local

CMD ["python", "-m", "autovlab"]

###############################################################################
# debug: install IPython and default to IPython shell
###############################################################################
FROM base as debug

RUN python -m pip install --user --no-warn-script-location --no-cache-dir IPython || true
CMD  ["python", "-m", "IPython"]
