# autovlab

`Auto-vLab`: Virtual infrastructure automation suite empowering infrastructure administrators to automate all the things!

# Core Features

## Template Management

* Curate a library of *hundreds* of VM templates ready to deploy on demand

## Infrastructure as Code

* *Describe* arbitrarily complex networks of VMs in *simple*, *human-readable* configuration files
* *Provision* and tear down virtual infrastructure based on your declarative config


## Infrastructure Management

* *Manage* provisioned infrastructure and automate hypervisor operations (e.g. snapshots, power on / off, etc.)


# Focus

`Auto-vLab` focuses on *self-hosted* infrastructure and is designed for administrators who

* have their own hardware / self-hosted virtual infrastrcture
* are running a supported hypervisor (i.e. `vSphere`)
* want granual control over VM OS flavor and versions
* want access to a giant, ready-to-deploy library of VM templates
* want maintainable, understandable, repeatable infrastructure deployments leveraging IAC
* want to automate provisioning on-demand / ephemeral infrastructure
* want hybrid ephemeral / long-running infrastructure
* want to automate infrastructure managment
* want granual control over isolating infrastructure segments

These core goals make `Auto-vLab` particularly well-suited for

* [Cyber Range](https://www.nist.gov/system/files/documents/2018/02/13/cyber_ranges.pdf) administrators
* Cybersecurity competition (e.g. [CCDC](https://www.nationalccdc.org/) / [CPTC](https://www.nationalcptc.org/) -like) administrators
* Student teams training for cybersecurity competitions
* Educators providing virtual infrasture to students
* [Red Teams](https://csrc.nist.gov/glossary/term/Red_Team) with self-hosted Red Team infrastructure
* [Red Teams](https://csrc.nist.gov/glossary/term/Red_Team) who want to recreate target networks with matching OS versions
* Software development teams with self-hosted infrastructure
* [VMUG](https://www.vmug.com/home) members / Homelab enthusiasts (with near-enterprise setups)

# Prerequisites

* `vSphere` with `vCenter` 6.7+
* Administrator access to `vSphere`
* An (isolated / hardened) admin system with network access to `vCenter` with [Docker](https://docs.docker.com/get-docker/) installed


# FAQ

## Why `vSphere` / `vCenter` instead of (free hypervisor, like `ESXi` / `Xen` / `Proxmox` / etc.)?

* VMware still dominates the self-hosted virtualization market
* Free ESXi does not support the hypervisor API needed to do the automation
* `vCenter` has a fairly mature API with lots of [community examples](https://github.com/vmware/pyvmomi-community-samples)
* It would be awesome to have a fully `FOSS` solution one day, but there are a *lot* of hypervisor-specific features that `Auto-vLab` will likely remain focused on `vSphere`

## Why not (cloud provider, AWS / Azure / Google Cloud / Digitial Ocean / Linode / etc. ) ?

Cloud providers have their own tools to enable automation, but they also have rules and limitations.

For example, most will not allow demonstrating cybersecurity attacks, hosting or executing 'malware'.

Also, cloud providers are walled gardens where
* The 'supported' VMs are extremely limited
* Storage / compute is usually not 'free'
* Uploading custom `OVA` / `OVF`s to run a custom image usually requires manual eforts

Together, this greatly discourages maintaining a large, diverse library of ready-to-deploy images.

Additonally, there are privacy concerns / potential information leakage that could prevent some users form using a public cloud.

## Why not (container orchestrator, Kubernetes, Docker Swam, docker-compose, OpenShift, etc.) ?

`Auto-vLab` is not trying to solve the same problem that container orchestrators solve.

*If you want to deploy production software and can properly containerize it, use a container orchestrator*

`Auto-vLab` is a bit like '`docker-compose` for VMs', but the focus is different.

* `docker-compose` / other declarative container `YAML` configuration describes an abstract *application* / *service* stack that should be created
* `Auto-vLab` configs describe an abstract *collection of VMS* / *virtual network topology* that should be created

`Auto-vLab` is for creating *VMs* (running the OS versions you requested), firewalls and virtual networks (isolated by virtual VLANs) arranged in the topology you defined.

(You can use `Auto-vLab` to deploy the VM nodes you use to run a container orchestrator, though.)
