import os
import typing
import traceback
from pathlib import Path
from contextlib import contextmanager

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.exc import DetachedInstanceError

from autovlab import db_logger
from autovlab.app import autovlab_app

import autovlab.utils as utils


def get_db_uri():
    db_name = utils.get_secret("AUTOVLAB_DB_NAME")
    db_user = utils.get_secret("AUTOVLAB_DB_USER")
    db_host = utils.get_secret("AUTOVLAB_DB_HOST")
    db_port = utils.get_secret("AUTOVLAB_DB_PORT", default=5432)
    db_pass = utils.get_secret("AUTOVLAB_DB_PASSWORD")
    uri = f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
    db_logger.debug(uri)
    return uri


autovlab_app.config["SQLALCHEMY_DATABASE_URI"] = get_db_uri()
autovlab_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

autovlab_db = None
try:
    autovlab_db = SQLAlchemy(autovlab_app)
except Exception as e:
    db_logger.error(traceback.format_exc())
    db_logger.error("Failed to connect to database: {e}")


@contextmanager
def transaction():
    try:
        yield
        autovlab_db.session.commit()
    except Exception as e:
        db_logger.debug(traceback.format_exc())
        db_logger.error(f"{type(e)}: {e}")
        autovlab_db.session.rollback()
        raise


def db_add(model_obj) -> bool:
    res = False
    try:
        db_logger.debug(model_obj)
        with transaction():
            autovlab_db.session.add(model_obj)
        res = True
    except Exception as e:
        db_logger.error(f"Failed to add {model_obj}")

    return res


def db_delete(model_obj) -> bool:
    res = False
    try:
        db_logger.debug(model_obj)
        with transaction():
            autovlab_db.session.delete(model_obj)
        res = True
    except Exception as e:
        db_logger.error(f"Failed to delete {model_obj}")

    return res


def db_update(model_obj, update: dict) -> bool:
    db_logger.debug(model_obj)
    db_logger.debug(update)
    for key, val in update.items():
        if hasattr(model_obj, key):
            setattr(model_obj, key, val)
        else:
            raise KeyError(f"{model_obj} has no attribute {key}")

    return db_add(model_obj)


class BaseModel(autovlab_db.Model):
    __abstract__ = True

    def __repr__(self) -> str:
        return self._repr(id=self.id)

    def _repr(self, **fields: typing.Dict[str, typing.Any]) -> str:
        """
        Helper for __repr__
        """
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f"{key}={field!r}")
            except DetachedInstanceError:
                field_strings.append(f"{key}=DetachedInstanceError")
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"
