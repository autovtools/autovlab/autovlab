from autovlab.app import autovlab_app
from autovlab.security import authorizations, security_type

# Import all the api's, which need namespaces and models
import autovlab.api.admin
import autovlab.api.auth
import autovlab.api.projects
import autovlab.api.templates
import autovlab.api.jobs
