from flask import request

from flask_restx import Resource
from flask_accepts import accepts, responds
from flask_jwt_extended import jwt_required, get_jwt_identity, get_jwt_claims

from autovlab import admin_ns, admin_logger, abort

from autovlab.db import db_add, db_delete, db_update
from autovlab.security import acl_required
from autovlab.models.admin import *


@admin_ns.route("/users")
class UsersResource(Resource):
    """Manage Users"""

    @admin_ns.doc("get_users")
    @responds(
        api=admin_ns,
        schema=UserSchema(many=True, exclude=["password", "password_hash"]),
    )
    @acl_required
    def get(self):
        """Get list of users"""
        users = UserModel.query.all()
        admin_logger.debug(users)
        return users

    @admin_ns.doc("create_user")
    @accepts(api=admin_ns, schema=NewUserSchema)
    @responds(api=admin_ns, schema=UserSchema)
    @acl_required
    def post(self):
        """Create a new user"""
        new_user = request.parsed_obj

        admin_logger.debug(new_user)

        user_model = UserModel(
            username=new_user.username,
            password=new_user.password,
            locked=new_user.locked,
            roles=new_user.roles,
            groups=new_user.groups,
        )

        # TODO: check if username is takem
        if db_add(user_model):
            admin_logger.debug(f"Added user {user_model.username}")
            schema = UserSchema(exclude=["password", "password_hash"])
            added_user = schema.dump(user_model)

            if new_user._generated_password:
                # We generated a password and we need to return it to the user
                admin_logger.debug(f"Generated password: {new_user.password}")
                added_user["password"] = new_user.password

            return added_user
        else:
            details = f"Failed to create user: {user_model.username}"
            abort(400, details=details, logger=admin_logger)


@admin_ns.route("/users/<int:user_id>")
class UserResource(Resource):
    """Manage a User"""

    @admin_ns.doc("get_user")
    @responds(api=admin_ns, schema=UserSchema(exclude=["password_hash"]))
    @acl_required
    def get(self, user_id):
        """Get a User"""
        user = UserModel.query.filter_by(id=user_id).first_or_404()
        admin_logger.debug(user)

        return user

    @admin_ns.doc("update_user")
    @accepts(api=admin_ns, schema=UserSchema(exclude=["id"]))
    @responds(api=admin_ns, schema=UserSchema(exclude=["password_hash"]))
    @acl_required
    def put(self, user_id):
        """Update a User"""
        user = UserModel.query.filter_by(id=user_id).first_or_404()
        admin_logger.debug(user)
        allowed = ["username", "password", "locked"]
        update = utils.get_update_dict(request.parsed_obj, allowed)

        requested = request.parsed_obj.roles
        if requested:
            admin_logger.debug("requested_roles: {requested}")
            roles = UserModel.query.filter(Role.name.in_(requested))
            update["roles"] = roles

        if requested:
            requested = request.parsed_obj.groups
            admin_logger.debug("requested_groups: {requested}")
            groups = UserModel.query.filter(Role.name.in_(requested))
            update["groups"] = groups

        if db_update(user, update):
            return user
        else:
            details = f"Failed to update {user}"
            return abort(400, details, logger=admin_logger)

    @admin_ns.doc("delete_user")
    @responds(api=admin_ns, schema=UserSchema(exclude=["password_hash"]))
    @acl_required
    def delete(self, user_id):
        """Delete a User"""
        user = UserModel.query.filter_by(id=user_id).first_or_404()
        admin_logger.debug(user)

        if not db_delete(user):
            details = f"Failed to delete user: {user.username}"
            abort(400, details=details, logger=admin_logger)

        return f"deleted user {user.username}", 200


@admin_ns.route("/groups")
class GroupsResource(Resource):
    """Manage Users"""

    @admin_ns.doc("get_groups")
    @responds(api=admin_ns, schema=GroupSchema(many=True))
    @acl_required
    def get(self):
        """Get list of groups"""
        users = GroupModel.query.all()
        admin_logger.debug(users)
        return users

    @admin_ns.doc("create_group")
    @accepts(api=admin_ns, schema=GroupSchema())
    @responds(api=admin_ns, schema=GroupSchema)
    @acl_required
    def post(self):
        """Create a new group"""
        group = request.parsed_obj

        if db_add(group):
            admin_logger.debug(f"Added group {group.name}")
            return GroupSchema().dump(group)
        else:
            details = f"Failed to create group: {group.name}"
            admin_logger.warning(details)
            abort(400, details=details)


@admin_ns.route("/roles")
class RolesResource(Resource):
    """Manage Roles"""

    @admin_ns.doc("get_roles")
    @responds(api=admin_ns, schema=RoleSchema(many=True))
    @acl_required
    def get(self):
        """Get list of roles"""
        roles = RoleModel.query.all()
        admin_logger.debug(roles)
        return roles

    @admin_ns.doc("create_role")
    @accepts(api=admin_ns, schema=RoleSchema())
    @responds(api=admin_ns, schema=RoleSchema)
    @acl_required
    def post(self):
        """Create a new role"""
        role = request.parsed_obj

        # TODO: check if username is takem
        if db_add(role):
            admin_logger.debug(f"Added role {role.name}")
            return RoleSchema().dump(role)
        else:
            details = f"Failed to create role: {role.name}"
            admin_logger.warning(details)
            abort(400, details=details)
