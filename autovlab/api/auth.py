from flask import request
from flask_restx import Resource, abort
from flask_accepts import accepts, responds
from flask_jwt_extended import (
    jwt_required,
    jwt_optional,
    jwt_refresh_token_required,
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jwt_claims,
)

from autovlab import auth_ns, auth_logger
from autovlab.security import jwt, acl_required, check_password, INVALID_USER_HASH
from autovlab.models.auth import *
from autovlab.models.admin import UserModel

# Using the user_claims_loader, we can specify a method that will be
# called when creating access tokens, and add these claims to the said
# token. This method is passed the identity of who the token is being
# created for, and must return data that is json serializable
@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    # TODO: Load roles from database
    # Permission changes will not apply until user gets new token
    return {
        "roles": [identity],
        "groups": ["PTDSL"],
        "projects": ["Demo"],
    }


@auth_ns.route("/login")
class LoginResource(Resource):
    """Login and obtain tokens"""

    @auth_ns.doc("login")
    @accepts(api=auth_ns, schema=LoginRequestSchema)
    @responds(api=auth_ns, schema=LoginResponseSchema)
    def post(self):
        """Login"""
        req = request.parsed_obj
        auth_logger.debug(f"LOGIN {req.username}")

        user = UserModel.query.filter_by(username=req.username).first()
        success = False
        if user:
            success = check_password(req.password, user.password_hash)
        else:
            # User is invalid, but do a hash check anyway to mitigate
            #   timing attacks
            # Leave success false
            check_password("INVALID", INVALID_USER_HASH)

        if not success:
            auth_logger.warning(f"LOGIN {req.username}: failed")
            details = "Invalid username or password"
            return abort(401, details=details)

        auth_logger.debug(f"LOGIN {req.username}: success")

        resp = LoginResponse(
            access_token=create_access_token(req.username),
            refresh_token=create_refresh_token(req.username),
        )
        auth_logger.debug(resp)
        return resp

    @auth_ns.doc("logout")
    @responds(api=auth_ns, schema=LoginStatusSchema)
    @jwt_optional
    def delete(self):
        """Logout"""
        # JWT is stateless, so the token stays valid util it expires
        #   But, we can always redirect the client and delete the token
        #   on the client side for a familiar UX
        user = get_jwt_identity()

        resp = LoginStatus(status="logged_out", username=None)
        if user:
            resp = LoginStatus(status="logged_out", username=user)

        auth_logger.debug(resp)
        return resp

    @auth_ns.doc("refresh")
    @responds(api=auth_ns, schema=RefreshResponseSchema)
    @jwt_refresh_token_required
    def put(self):
        """Refresh access_token"""
        current_user = get_jwt_identity()
        resp = RefreshResponse(access_token=create_access_token(current_user))
        return resp

    @auth_ns.doc("check_login_status")
    @responds(api=auth_ns, schema=LoginStatusSchema)
    @jwt_optional
    def get(self):
        """Check if currently logged in"""
        user = get_jwt_identity()

        resp = LoginStatus(status="logged_out", username=None)
        if user:
            resp = LoginStatus(status="logged_in", username=user)
            auth_logger.debug(get_jwt_claims())

        auth_logger.debug(resp)
        return resp
