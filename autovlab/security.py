import secrets
import bcrypt
from functools import wraps

from flask import request
from flask_restx import abort

from flask_jwt_extended import (
    JWTManager,
    verify_jwt_in_request,
    jwt_required,
    jwt_refresh_token_required,
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jwt_claims,
)

from autovlab import security_logger
from autovlab.app import autovlab_app

# Default is 12, so 14 must be at least *2* better
BCRYPT_ROUNDS = 14

# Number of random bytes in generated passwords
TOKEN_BYTES = 32

# Setup the Flask-JWT-Extended extension
# On restart, rotate the key
autovlab_app.config["JWT_SECRET_KEY"] = secrets.token_hex(64)
security_type = "BearerAuth"
jwt = JWTManager(autovlab_app)

authorizations = {
    "BearerAuth": {"type": "apiKey", "in": "header", "name": "Authorization"},
}


def generate_password(num_bytes=TOKEN_BYTES):
    return secrets.token_hex(num_bytes)


def hash_password(password):
    if not isinstance(password, bytes):
        password = password.encode()
    salt = bcrypt.gensalt(BCRYPT_ROUNDS)
    return bcrypt.hashpw(password, salt).decode()


def check_password(password, password_hash):
    if not isinstance(password, bytes):
        password = password.encode()

    if not isinstance(password_hash, bytes):
        password_hash = password_hash.encode()

    actual = bcrypt.hashpw(password, password_hash)
    return secrets.compare_digest(actual, password_hash)


# To prevent timing attacks, even invalid usernames need to cause a hash compare
INVALID_USER_HASH = bcrypt.hashpw("INVALID".encode(), bcrypt.gensalt(BCRYPT_ROUNDS))


def get_group_paths(groups):
    return [f"/projects/{group}/" for group in groups]


def group_allowed(groups):
    return request.path.startswith(tuple(path for path in get_group_paths(groups)))


def project_allowed(projects):
    return request.path.startswith(tuple(projects))


def curator_allowed():
    return request.path.startswith("/templates")


def is_authorized():
    verify_jwt_in_request()
    claims = get_jwt_claims()
    security_logger.debug(claims)

    roles = claims["roles"]
    groups = claims["groups"]
    projects = claims["projects"]

    user = get_jwt_identity()

    if "admin" in roles:
        # Admin is always authorized
        security_logger.debug(f"[{user}] admin: ALLOW {request.path}")
        return True
    elif "provisioner" in roles and group_allowed(groups) and project_allowed(projects):
        security_logger.debug(f"[{user}] provisioner: ALLOW {request.path}")
        return True
    elif "user" in roles and project_allowed(projects):
        security_logger.debug(f"[{user}] user: ALLOW {request.path}")
        return True
    elif "curator" in roles and curator_allowed():
        security_logger.debug(f"[{user}] curator: ALLOW {request.path}")
        return True

    security_logger.warning(f"[{user}] DENY {request.path}")
    return False


def acl_required(fn):
    """Require ACL check to access endpoint"""

    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        if is_authorized():
            return fn(*args, **kwargs)
        else:
            return abort(403)

    return wrapper
