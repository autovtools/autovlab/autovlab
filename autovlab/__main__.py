import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--debug-shell",
    action="store_true",
    help="Start a debug shell",
)

args = parser.parse_args()
print(args)
if args.debug_shell:
    import IPython  # pylint: disable=import-error

    IPython.embed()

import autovlab.app

autovlab.app.run()
