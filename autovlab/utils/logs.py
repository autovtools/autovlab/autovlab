"""Basic utils to simplify + standarize logging

"""
import logging

import coloredlogs


def _add_handler(logger, handler, fmt, log_level):
    handler.setLevel(log_level)
    handler.setFormatter(fmt)
    logger.addHandler(handler)


# name=self.__class__.__name__


def get_logger(name=None, log_level=logging.INFO, log_file=None, show_line_info=False):
    logger = logging.getLogger(name)
    if len(logger.handlers) == 0:
        logger.setLevel(log_level)
        logger.propagate = False

        extra = ""
        # Add line info for debug logging
        if show_line_info:
            extra = " - %(filename)s:%(funcName)s:%(lineno)d"

        fmt_str = (
            "%(asctime)s" " - %(levelname)s" " - %(name)s" + extra + " - %(message)s"
        )
        # Set up Console Handler (prints events to STDERR)
        coloredlogs.install(fmt=fmt_str, level=log_level, logger=logger)

        if log_file:
            fmt = logging.Formatter(fmt_str)
            # Set up File Handler (prints events to STDOUT)
            _add_handler(logger, logging.FileHandler(log_file), fmt, log_level)
    return logger


def get_subprocess_formatter(subprocess_name):
    fmt_str = f"[{subprocess_name}] %(message)s"
    return logging.Formatter(fmt_str)


def apply_formatter(logger, fmt):
    for handler in logger.handlers:
        handler.formatter = fmt
