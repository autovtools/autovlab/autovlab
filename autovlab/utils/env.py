import os
from pathlib import Path


def get_secret(name, default="autovlab", strip_newline=True):
    if name not in os.environ:
        name += "_FILE"

    if name not in os.environ:
        return default

    val = os.environ[name]
    if name.endswith("_FILE"):
        text = Path(val).read_text()
        if strip_newline:
            text = text.rstrip()
        return text
    else:
        return val
