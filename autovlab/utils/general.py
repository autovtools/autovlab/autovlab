def get_update_dict(obj, attrs):
    res = {}
    for attr in attrs:
        val = getattr(obj, attr)
        if val:
            res[attr] = val
    return res
