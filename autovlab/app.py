from flask import Flask
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow

autovlab_app = Flask(__name__)


def run():
    autovlab_app.run(debug=True, host="0.0.0.0")
