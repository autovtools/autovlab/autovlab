# Loggers for each api
import logging
import autovlab.utils as utils

LOG_LEVEL = logging.DEBUG
DEBUG_LINE_INFO = True

logger = utils.get_logger(
    "AUTOVLAB", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)
db_logger = utils.get_logger("DB", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO)
security_logger = utils.get_logger(
    "SECURITY", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)

admin_logger = utils.get_logger(
    "ADMIN", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)
auth_logger = utils.get_logger(
    "AUTH", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)
projects_logger = utils.get_logger(
    "PROJECTS", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)
templates_logger = utils.get_logger(
    "TEMPLATES", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)
jobs_logger = utils.get_logger(
    "JOBS", log_level=LOG_LEVEL, show_line_info=DEBUG_LINE_INFO
)

from flask_restx import Api
from flask_restx import abort as _abort
from flask_sqlalchemy import SQLAlchemy


def abort(status_code, details, logger=None, **kwargs):
    if logger:
        if status_code in [400, 500]:
            logger.error(details)
        else:
            logger.warning(details)

    return _abort(details=details, **kwargs)


import autovlab.security
from autovlab.app import autovlab_app
from autovlab.db import autovlab_db

## app and db must be initialed first
# autovlab.app.ma = Marshmallow(autovlab_app)

# Create an API instance that requires JWT auth
autovlab_api = Api(
    autovlab_app,
    version="0.1",
    title="Auto-vLab API",
    description="An API for controlling Auto-vLab",
    authorizations=autovlab.security.authorizations,
    security=autovlab.security.security_type,
)

# Namespaces for each top level API
auth_ns = autovlab_api.namespace("auth", description="User authentication")
projects_ns = autovlab_api.namespace("projects", description="Project management")
templates_ns = autovlab_api.namespace("templates", description="Template management")
jobs_ns = autovlab_api.namespace("jobs", description="Jobs management")

admin_ns = autovlab_api.namespace("admin", description="Auto-vLab Administration")

import autovlab.models
import autovlab.api

from autovlab.db import autovlab_db

autovlab_db.create_all()
