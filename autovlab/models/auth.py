from dataclasses import dataclass

from marshmallow import post_load

from autovlab.models import ma
from autovlab import auth_ns
from autovlab.models import ma

"""

Dataclasses

"""


@dataclass
class LoginStatus:
    status: str
    username: str


@dataclass
class LoginRequest:
    username: str
    password: str


@dataclass
class LoginResponse:
    access_token: str
    refresh_token: str


@dataclass
class RefreshResponse:
    access_token: str


"""

Schemas

"""


class LoginStatusSchema(ma.Schema):
    status = ma.String()
    username = ma.String()

    @post_load
    def make_obj(self, data, **kwargs):
        return LoginStatus(**data)


class LoginRequestSchema(ma.Schema):
    username = ma.String()
    password = ma.String()

    @post_load
    def make_obj(self, data, **kwargs):
        return LoginRequest(**data)


class LoginResponseSchema(ma.Schema):
    access_token = ma.String()
    refresh_token = ma.String()

    @post_load
    def make_obj(self, data, **kwargs):
        return LoginResponse(**data)


class RefreshResponseSchema(ma.Schema):
    access_token = ma.String()

    @post_load
    def make_obj(self, data, **kwargs):
        return RefreshResponse(**data)
