from flask_marshmallow import Marshmallow

from autovlab.app import autovlab_app as app
from autovlab.db import autovlab_db as db

# app and db must be initialed first
ma = Marshmallow(app)

import autovlab.models.admin
import autovlab.models.auth
