from dataclasses import dataclass, field

from marshmallow import post_load, validates, ValidationError
from sqlalchemy import event

from autovlab.models import ma
from autovlab.security import hash_password, generate_password

from autovlab.db import BaseModel
from autovlab.db import autovlab_db as db

import autovlab.utils as utils

"""
DB models

# https://flask-sqlalchemy.palletsprojects.com/en/2.x/models
"""

group_memberships = db.Table(
    "group_memberships",
    db.Column("group_id", db.Integer, db.ForeignKey("groups.id"), primary_key=True),
    db.Column("user_id", db.Integer, db.ForeignKey("users.id"), primary_key=True),
)

role_memberships = db.Table(
    "role_memberships",
    db.Column("role_id", db.Integer, db.ForeignKey("roles.id"), primary_key=True),
    db.Column("user_id", db.Integer, db.ForeignKey("users.id"), primary_key=True),
)


class GroupModel(BaseModel):
    __tablename__ = "groups"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    protected = db.Column(db.Boolean())
    default = db.Column(db.Boolean(), default=False)

    def __repr__(self) -> str:

        return self._repr(
            id=self.id,
            name=self.name,
        )


class RoleModel(BaseModel):
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    built_in = db.Column(db.Boolean(), default=False)

    def __repr__(self) -> str:

        return self._repr(
            id=self.id,
            name=self.name,
            built_in=self.built_in,
        )


class UserModel(BaseModel):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(255))
    locked = db.Column(db.Boolean(), default=False)
    groups = db.relationship(
        "GroupModel",
        secondary=group_memberships,
        lazy="subquery",
        backref=db.backref("user", lazy="joined"),
    )
    roles = db.relationship(
        "RoleModel",
        secondary=role_memberships,
        lazy="subquery",
        backref=db.backref("user", lazy="joined"),
    )
    # projects = db.relationship("Project", backref="user", lazy=True)

    @property
    def password(self):
        raise AttributeError("password not readable")

    @password.setter
    def password(self, password):
        self.password_hash = hash_password(password)

    def __repr__(self) -> str:

        return self._repr(
            id=self.id,
            username=self.username,
            locked=self.locked,
            groups=self.groups,
            roles=self.roles,
        )


"""
Dataclasses
"""


@dataclass
class User:
    username: str
    password: str = field(repr=False)
    roles: list
    groups: list
    projects: list
    locked: bool
    id: int = field(default=None)
    _generated_password: bool = field(default=False)


@dataclass
class Group:
    id: int
    name: str
    protected: bool
    default: bool
    built_in: bool
    projects: list
    users: list


@dataclass
class Role:
    id: int
    name: str
    built_in: bool
    users: list


"""
Schemas
"""


class GroupSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = GroupModel
        # load_instance = True
        # include_fk = True
        # include_relationships = True

    @validates("name")
    def validate_name(self, name):
        existing = self.query.filter_by(name=name).first()
        if existing:
            raise ValidationError(f"group '{name}' already exists!")

    @post_load
    def make_obj(self, data, **kwargs):
        # data["_id"] = None
        return Group(**data)


class RoleSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = RoleModel
        # load_instance = True
        # include_fk = True
        # include_relationships = True

    @validates("name")
    def validate_username(self, name):
        existing = self.query.filter_by(name=name).first()
        if existing:
            raise ValidationError(f"role '{name}' already exists!")

    @post_load
    def make_obj(self, data, **kwargs):
        # data["_id"] = None
        return Role(**data)


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserModel
        # load_instance = True
        # include_fk = True
        include_relationships = True

    password = ma.String()
    roles = ma.List(ma.Nested(RoleSchema))
    groups = ma.List(ma.Nested(GroupSchema))

    @post_load
    def make_obj(self, data, **kwargs):
        user = User(**data)
        return user


class NewUserSchema(ma.Schema):
    username = ma.String()
    password = ma.String()
    locked = ma.Boolean()

    roles = ma.List(ma.String())
    groups = ma.List(ma.String())

    @validates("username")
    def validate_username(self, username):
        existing = UserModel.query.filter_by(username=username).first()
        if existing:
            raise ValidationError(f"username '{username}' already exists!")

    @post_load
    def make_obj(self, data, **kwargs):
        data["roles"] = get_roles(data["roles"])
        data["groups"] = get_groups(data["groups"])

        data["projects"] = []

        if "locked" not in data["projects"]:
            data["locked"] = False

        if "password" not in data or not data["password"]:
            data["password"] = generate_password()
            data["_generated_password"] = True
        else:
            data["_generated_password"] = False
        user = User(**data)
        return user


"""
Default  data
"""


def insert_default_roles(target, connection, **kw):
    roles = [
        dict(name="admin", built_in=True),
        dict(name="provisioner", built_in=True),
        dict(name="curator", built_in=True),
        dict(name="user", built_in=True),
    ]
    connection.execute(target.insert(), *roles)


def insert_default_groups(target, connection, **kw):
    groups = [
        dict(name="vLAB", built_in=True, protected=False, default=True),
        dict(name="INFRA", built_in=True, protected=True, default=False),
    ]
    connection.execute(target.insert(), *groups)


def insert_default_users(target, connection, **kw):
    default_user = utils.get_secret("AUTOVLAB_USER")
    default_password = utils.get_secret("AUTOVLAB_PASSWORD")
    password_hash = hash_password(default_password)
    user = dict(
        username=default_user,
        password_hash=password_hash,
        roles=[1],
        groups=[],
        locked=False,
    )
    connection.execute(target.insert(), user)


def insert_default_role_memberships(target, connection, **kw):
    # Give the first / admin user admin privs
    connection.execute(target.insert(), dict(role_id=1, user_id=1))


###############################################################################
# DB Initialization
###############################################################################
event.listen(RoleModel.__table__, "after_create", insert_default_roles)
event.listen(GroupModel.__table__, "after_create", insert_default_groups)
event.listen(UserModel.__table__, "after_create", insert_default_users)
event.listen(role_memberships, "after_create", insert_default_role_memberships)
###############################################################################

###############################################################################
# Helper functions
###############################################################################
def get_objs_by_name(model, names, field="name"):
    objs = []
    for name in names:
        filter_args = {field: name}
        print(f"Filter: {filter_args}")
        obj = model.query.filter_by(**filter_args).first_or_404()
        print(obj)
        objs.append(obj)
    return objs


def get_roles(names):
    return get_objs_by_name(RoleModel, names)


def get_groups(names):
    return get_objs_by_name(GroupModel, names)


###############################################################################
