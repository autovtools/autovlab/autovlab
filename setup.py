from pathlib import Path
from setuptools import setup, find_packages


def get_version():
    path = Path("VERSION")
    if path.is_file():
        return path.read_text()
    return "0.0.0"


# WARNING: depends on
#   terradactsl
#   terrashell
#   packiantor
setup(
    name="autovlab",
    version=get_version(),
    packages=find_packages(),
    install_requires=[
        "bcrypt",
        "coloredlogs",
        "flask",
        "flask-restx",
        "flask-jwt-extended",
        "flask-marshmallow",
        "marshmallow-sqlalchemy",
        "flask-accepts",
        "psycopg2-binary",
    ],
)
